(function ($) {
  Drupal.behaviors.mediaPrivacy = {
    attach: function (context, settings) {

      var mediaPrivacy = $(context).find('.media-privacy__wrapper');

      mediaPrivacy.each(function() {

        var embed = Drupal.settings.mediaPrivacy[this.id];

        $(this).find('.media-privacy__overlay__link').bind('click', function(event) {
          event.preventDefault();
          $($(this).parent().parent().parent()).addClass('loaded');
          $(this).parent().parent().parent().html(embed.embed);
          $(this).unbind('click');

        });

        $(this).bind('mouseover touch', function() {
          var privacyPreview = $(this).find('.media-privacy__preview');
          var privacyOverlay = $(this).find('.media-privacy__overlay');
          $(privacyPreview).addClass('hide-preview');
          $(privacyOverlay).addClass('show-overlay');
        });
        $(this).bind('mouseout', function() {
          var privacyPreview = $(this).find('.media-privacy__preview');
          var privacyOverlay = $(this).find('.media-privacy__overlay');
          $(privacyPreview).removeClass('hide-preview');
          $(privacyOverlay).removeClass('show-overlay');
        });

      });
    }
  };
}(jQuery));