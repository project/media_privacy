<?php

/**
 * @file
 * Default display configuration for the media privacy video formatter plugin.
 */

/**
 * Implements hook_file_default_displays().
 */
function media_privacy_video_example_file_default_displays() {
  $file_displays = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__default__media_privacy_video';
  $file_display->weight = -100;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'implementation' => 0,
    'text' => t('You need to click the link below to activate this video. We want to inform you that by activating the video, possibly sensitive data is sent to the video provider.'),
    'link' => t('I understand & agree'),
  );
  $file_displays['video__default__media_privacy_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__teaser__media_privacy_video';
  $file_display->weight = -100;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'implementation' => 0,
    'text' => t('You need to click the link below to activate this video. We want to inform you that by activating the video, possibly sensitive data is sent to the video provider.'),
    'link' => t('I understand & agree'),
  );
  $file_displays['video__teaser__media_privacy_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__preview__media_privacy_video';
  $file_display->weight = -100;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'implementation' => 0,
    'text' => t('You need to click the link below to activate this video. We want to inform you that by activating the video, possibly sensitive data is sent to the video provider.'),
    'link' => t('I understand & agree'),
  );
  $file_displays['video__preview__media_privacy_video'] = $file_display;

  return $file_displays;
}
