<?php

/**
 * @file
 * Theme and preprocess functions for media privacy video.
 */

/**
 * Preprocess function for theme('media_privacy_video').
 */
function media_privacy_video_preprocess_media_privacy_video(&$variables) {
  media_privacy_preprocess_base($variables);
}
