<?php

/**
 * @file
 * Template file for theme('media_privacy_audio').
 *
 * Variables available:
 * - $text: The text that will be displayed to the user explained explaining the
 *   extra click needed.
 * - $link: The text of the link.
 * - $preview_text: The text on the embed's preview.
 * - $preview_image_src: The url to the preview's image file.
 * - $unique_identifier: The unique identifier of this media audio privacy
 *   instance.
 * - $preview: The preview to display instead of the actual audio embed.
 * - $dimensions: The configured width and height of the embed as inline css.
 * - $width: The configured width in px as integer.
 * - $height: The configured height in px as integer.
 * - $static: Whether or not the static mode is enabled.
 * - $static_image: Static preview image.
 */

?>

<?php if (($static === 1) && isset($static_image)) { ?>

    <div class="<?php print $classes; ?> media-privacy-audio-<?php print $id; ?>  media-privacy__wrapper" id="<?php print $unique_identifier ?>" <?php if (isset($dimensions)): print 'data-initialheight="' . $height . '" data-initialwidth="' . $width. '"'; endif; ?> <?php if (isset($static_image)): print 'style="background-image:url(' . $static_image . ')"'; endif; ?>>

        <div class="media-privacy__preview">

          <?php if (isset($preview_text)) { ?>
              <div class="media-privacy__preview__info">
                  <p>
                    <?php print render($preview_text); ?>
                  </p>
              </div>
          <?php } ?>

        </div>

        <div class="media-privacy__overlay">

            <div class="media-privacy__overlay__content">
                <a class="media-privacy__overlay__link" href="#"><?php print $link ?></a>
                <p><?php print $text ?></p>
            </div>
        </div>

    </div>

<?php } else { ?>

    <div class="<?php print $classes; ?> media-privacy-audio-<?php print $id; ?>  media-privacy__wrapper" id="<?php print $unique_identifier ?>" <?php if (isset($dimensions)): print 'data-initialheight="' . $height . '" data-initialwidth="' . $width. '"'; endif; ?> <?php if (isset($preview_image_src)): print 'style="background-image:url(' . $preview_image_src . ')"'; endif; ?>>

        <div class="media-privacy__preview">

          <?php if (isset($preview_text)) { ?>
              <div class="media-privacy__preview__info">
                  <p>
                    <?php print render($preview_text); ?>
                  </p>
              </div>
          <?php } ?>

        </div>

        <div class="media-privacy__overlay">

            <div class="media-privacy__overlay__content">
                <a class="media-privacy__overlay__link" href="#"><?php print $link ?></a>
                <p><?php print $text ?></p>
            </div>
        </div>

    </div>

<?php } ?>