<?php

/**
 * @file
 * Theme and preprocess functions for media privacy audio.
 */

/**
 * Preprocess function for theme('media_privacy_audio').
 */
function media_privacy_audio_preprocess_media_privacy_audio(&$variables) {
  media_privacy_preprocess_base($variables);
}
