<?php

/**
 * @file
 * Default display configuration for the media privacy audio formatter plugin.
 */

/**
 * Implements hook_file_default_displays().
 */
function media_privacy_audio_example_file_default_displays() {
  $file_displays = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__default__media_privacy_audio';
  $file_display->weight = -100;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'implementation' => 0,
    'text' => t('You need to click the link below to activate this audio. We want to inform you that by activating the audio, possibly sensitive data is sent to the audio provider.'),
    'link' => t('I understand & agree'),
  );
  $file_displays['audio__default__media_privacy_audio'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__teaser__media_privacy_audio';
  $file_display->weight = -100;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'implementation' => 0,
    'text' => t('You need to click the link below to activate this audio. We want to inform you that by activating the audio, possibly sensitive data is sent to the audio provider.'),
    'link' => t('I understand & agree'),
  );
  $file_displays['audio__teaser__media_privacy_audio'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__preview__media_privacy_audio';
  $file_display->weight = -100;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'implementation' => 0,
    'text' => t('You need to click the link below to activate this audio. We want to inform you that by activating the audio, possibly sensitive data is sent to the audio provider.'),
    'link' => t('I understand & agree'),
  );
  $file_displays['audio__preview__media_privacy_audio'] = $file_display;

  return $file_displays;
}
