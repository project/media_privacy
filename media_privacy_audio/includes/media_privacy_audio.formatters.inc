<?php

/**
 * @file
 * File formatters for media_privacy_audio embeds.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_privacy_audio_file_formatter_info() {
  return media_privacy_file_formatter_info_base('audio', 'media_privacy_audio', 'Audio privacy');
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_privacy_audio_file_formatter_audio_view($file, $display, $langcode) {

  // Get base formatter view structure.
  $elements = media_privacy_file_formatter_type_view_base($file, $display, $langcode, 'media_privacy_audio');

  // Add media privacy audio specific styling.
  $elements['#attached']['css'][] = drupal_get_path('module', 'media_privacy_audio') . '/css/media_privacy_audio.css';

  return $elements;
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_privacy_audio_file_formatter_audio_settings($form, &$form_state, $settings) {
  return media_privacy_file_formatter_type_settings_base($form, $form_state, $settings);
}
