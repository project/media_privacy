<?php

/**
 * @file
 * Utility media privacy file formatter.
 */

/**
 * To be used by submodule's implementation of hook_file_formatter_info().
 */
function media_privacy_file_formatter_info_base($type, $name, $label) {
  $formatters[$name] = array(
    'label' => t($label),
    'file types' => array($type),
    'default settings' => array(
      //'implementation' => 0,
      'text' => t('You need to click the link below to activate this @type. We want to inform you that by activating the $type, possibly sensitive data is sent to the $type provider.', ['@type' => $type]),
      'link' => t('I understand & agree'),
      'preview_text' => t('@type not loaded yet', ['@type' => ucfirst($type)]),
      'static' => 0,
      'static_image' => '',
    ),
    'view callback' => $name . '_file_formatter_' . $type . '_view',
    'settings callback' => $name . '_file_formatter_' . $type . '_settings',
    'mime types' => array($type . '/*'),
  );

  return $formatters;
}

/**
 * To be used by submodule's implementation of hook_file_formatter_FORMATTER_view().
 */
function media_privacy_file_formatter_type_view_base($file, $display, $langcode, $name) {
  $file_formatter_view = file_view_file($file, $display['settings']['displays'], $langcode);

  $unique_identifier = 'media_privacy_wrapper_' . drupal_random_key();

  $elements = array(
    '#theme' => $name,
    'text' => $display['settings']['text'],
    'link' => $display['settings']['link'],
    'preview_text' => $display['settings']['preview_text'],
    'static' => $display['settings']['static'],
    '#unique_identifier' => $unique_identifier,
  );

  // Generate static image url from saved fid.
  if (isset($display['settings']['static_image'])) {
    if (isset($display['settings']['static_image']) && ($display['settings']['static_image'] !== '')) {
      if (file_load($display['settings']['static_image'])->uri !== NULL) {
        $elements['static_image'] = file_create_url(file_load($display['settings']['static_image'])->uri);
      }
    }
  }

  if ($display['settings']['view_mode'] != 'preview') {
    $elements['preview'] = file_view_file($file, 'preview', $langcode);
  }

  $elements['#attached'] = array(
    'js' => array(
      array(
        'type' => 'setting',
        'data' => array(
          'mediaPrivacy' => array(
            $unique_identifier => array(
              'embed' => drupal_render($file_formatter_view),
            ),
          ),
        ),
      ),
      array(
        'type' => 'file',
        'data' => drupal_get_path('module', 'media_privacy') . '/js/media_privacy.js',
      ),
    ),
    'css' => array(
      drupal_get_path('module', 'media_privacy') . '/css/media_privacy.css',
    ),
  );

  // Get embed dimensions, if available.
  if (isset($file_formatter_view['#options'])) {
    $width = $file_formatter_view['#options']['width'] ? $file_formatter_view['#options']['width'] : NULL;
    $height = $file_formatter_view['#options']['height'] ? $file_formatter_view['#options']['height'] : NULL;
  }

  if (isset($width) && isset($height)) {
    $elements['width'] = $width;
    $elements['height'] = $height;
  }

  return $elements;
}

/**
 * To be used by submodule's implementation of hook_file_formatter_FORMATTER_settings().
 */
function media_privacy_file_formatter_type_settings_base($form, &$form_state, $settings) {
  $element = array();

  $element['text'] = array(
    '#title' => t('Message'),
    '#type' => 'textarea',
    '#default_value' => $settings['text'],
    '#description' => t('Enter the message to be displayed to the website visitors.'),
  );

  $element['link'] = array(
    '#title' => t('Link text'),
    '#type' => 'textfield',
    '#default_value' => $settings['link'],
    '#description' => t('Enter the text to be displayed as the confirmation link.'),
  );

  $element['preview_text'] = array(
    '#title' => t('Preview text'),
    '#type' => 'textfield',
    '#default_value' => $settings['preview_text'],
    '#description' => t("Enter the text to be displayed on the embed's preview."),
  );

  $options = [
    0 => t('Yes'),
    1 => t('No'),
  ];

  $element['static'] = [
    '#title' => t('Use static image instead of dynamic previews'),
    '#type' => 'checkbox',
    '#default_value' => $settings['static'],
    '#options' => $options,
    '#description' => t('Select the mode the privacy plugin operates in.'),
  ];

  $element['static_image'] = array(
    '#type' => 'managed_file',
    '#title' => t('Fallback picture'),
    '#description' => t('Allowed extensions: gif png jpg jpeg'),
    '#default_value' => $settings['static_image'],
    '#upload_location' => 'public://media_privacy_fallback_image/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      // Pass the maximum file size in bytes
      'file_validate_size' => array(file_upload_max_size()),
    ),
  );

  return $element;
}
