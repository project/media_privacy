CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage

INTRODUCTION
------------

_Media Privacy_ adds one- or two-click solution to media video embeds.
Instead of rendering an external embed directly, a static preview (configurable) is
displayed which will be replaced by the actual embed upon clicking on the
preview. That way the external provider cannot track the user unless the embed is
actually "activated".

INSTALLATION
------------

_Media Privacy_ can be installed via the standard Drupal installation
process (http://drupal.org/node/895232).

USAGE
-----
If the _Media: Video Privacy_ and/or _Media: Audio Privacy_ module is installed, 
a _Video Privacy_ and/or _Audio Privacy_ display will appear on the file display 
page (admin/structure/file-types/manage/[type]/file-display). Make sure that this
display is used first. The preview that will be shown initially instead of the
external embed can be configured by configuring the _Preview_ file view
mode (admin/structure/file-types/manage/[type]/file-display/preview).
Make sure that the _Audio Privacy_ and/or _Video Privacy_ display is disabled on 
the respective _Preview_ file view mode.